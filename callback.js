const fs = require("fs");
const path = require("path");

// Q1. Create a new file data.json
// {
//     "max": {
//         colors: ['Orange', 'Red']
//     }
// }

// Ans 1..
let object = {
  max: {
    colors: ["Orange", "Red"],
  },
};

fs.writeFile(
  path.join(__dirname, "./data.json"),
  JSON.stringify(object),
  (err) => {
    if (err) {
      console.log(err);
    } else {
      console.log("file created successfully");
    }
  }
);

// Q2. Copy the contents of the data.json into a new folder output/data.json and delete the original file.

// Ans 2..
fs.readFile(
  path.join(__dirname, "./data.json"),
  {
    encoding: "utf-8",
  },
  (err, data) => {
    if (err) {
      console.log(err);
    } else {
      fs.writeFile(path.join(__dirname, "./output/data.json"), data, (err) => {
        if (err) {
          console.log(err);
        } else {
          console.log("File created successfully");
        }
      });
    }
  }
);

// Q3. Write a function that takes a person name and fav color as parameters and writes to data.json file

// Ans 3..
function setNameColors(data, name, colors) {
  let obj = { ...data };
  // obj[name] = {};
  obj[name] = { colors: colors };
  return obj;
}

fs.readFile(
  path.join(__dirname, "./output/data.json"),
  {
    encoding: "utf-8",
  },
  (err, data) => {
    if (err) {
      console.log(err);
    } else {
      console.log(data);
      const nameColor = setNameColors(JSON.parse(data), "murphy", [
        "red",
        "green",
      ]);
      fs.writeFile(
        path.join(__dirname, "./output/data.json"),
        JSON.stringify(nameColor),
        (err) => {
          if (err) {
            console.log(err);
          } else {
            console.log("File created successfully");
          }
        }
      );
    }
  }
);

// (Do not replace the old content).
// Q4. Write a function that takes a person name and fav hobby as param and add that hobby as a separate key and write to data.json.
// (Do not replace the old content).

// Ans 4..
function setNameHobby(data, name, hobby) {
  let obj = { ...data };
  obj[name] = { hobby: hobby };
  return obj;
}

fs.readFile(
  path.join(__dirname, "./output/data.json"),
  {
    encoding: "utf-8",
  },
  (err, data) => {
    if (err) {
      console.log(err);
    } else {
      console.log(data);
      const nameColor = setNameHobby(JSON.parse(data), "murphy", [
        "singing",
        "dancing",
      ]);
      fs.writeFile(
        path.join(__dirname, "./output/data.json"),
        JSON.stringify(nameColor),
        (err) => {
          if (err) {
            console.log(err);
          } else {
            console.log("File created successfully");
          }
        }
      );
    }
  }
);
